package bsa.java.concurrency;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SaveImageResultDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.concurrent.CompletableFuture;

@SpringBootTest
public class ImageFileSystemTests {
    @Autowired
    private FileSystem fileSystem;

    @Test
    public void saveFile() {
        String imagePath = "/home/volodymyr/StudyProjects/AcademyBSA/Homework3-concurrency/hash-test/images/testImage.jpg";
        BufferedImage image = null;
        byte[] bytes = null;

        try (InputStream is = new FileInputStream(imagePath)) {
            bytes = is.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CompletableFuture<SaveImageResultDto> cf = fileSystem.saveFile(bytes);
    }
}
