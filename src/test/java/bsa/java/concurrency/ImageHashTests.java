package bsa.java.concurrency;

import bsa.java.concurrency.entity.Image;
import bsa.java.concurrency.image.HashService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@SpringBootTest
public class ImageHashTests {
    @Autowired
    private HashService hashService;

    @Test
    public void getHash() {
        String imagePath = "/home/volodymyr/StudyProjects/AcademyBSA/Homework3-concurrency/hash-test/images/testImage.jpg";
        BufferedImage image = null;
        byte[] bytes = null;

        try (InputStream is = new FileInputStream(imagePath)) {
            bytes = is.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CompletableFuture<Long> hash = hashService.getImageHash(bytes);
        try {
            System.out.println(hash.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
