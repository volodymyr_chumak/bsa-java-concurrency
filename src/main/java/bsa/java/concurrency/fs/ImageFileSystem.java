package bsa.java.concurrency.fs;

import bsa.java.concurrency.image.dto.SaveImageResultDto;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class ImageFileSystem implements FileSystem {
    @Value("${images.path}")
    private String imagesPath;

    @Override
    @Async
    public CompletableFuture<SaveImageResultDto> saveFile(byte[] file) {
        UUID id = UUID.randomUUID();
        Path fullFilePath = Path.of(imagesPath + "/" + id + ".jpg");

        createImagesFolderIfItNotExist().join();

        try {
            AsynchronousFileChannel fc = AsynchronousFileChannel.open(fullFilePath, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
            fc.write(ByteBuffer.wrap(file), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(new SaveImageResultDto(id, fullFilePath.toString()));
    }

    @Async
    public CompletableFuture<Void> createImagesFolderIfItNotExist() {
        File imagesFolder = new File(imagesPath);
        if (!imagesFolder.exists()) imagesFolder.mkdir();

        return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<Void> deleteFile(UUID imageId) {
        String imagePath = imagesPath + "/" + imageId + ".jpg";
        File image = new File(imagePath);
        image.delete();

        return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<Void> deleteAllFiles() {
        File imagesDirrectory = new File(imagesPath);
        try {
            FileUtils.cleanDirectory(imagesDirrectory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }
}
