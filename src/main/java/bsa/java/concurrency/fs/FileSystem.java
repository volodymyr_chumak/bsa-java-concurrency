package bsa.java.concurrency.fs;

import bsa.java.concurrency.image.dto.SaveImageResultDto;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface FileSystem {
    CompletableFuture<SaveImageResultDto> saveFile(byte[] file);

    CompletableFuture<Void> deleteFile(UUID imageId);

    CompletableFuture<Void> deleteAllFiles();
}
