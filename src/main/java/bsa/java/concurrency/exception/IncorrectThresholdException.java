package bsa.java.concurrency.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Threshold must be in (0, 1] !")
public class IncorrectThresholdException extends RuntimeException { }
