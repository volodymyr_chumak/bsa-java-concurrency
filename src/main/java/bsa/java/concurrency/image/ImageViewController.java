package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.ImageNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

@Controller
public class ImageViewController {
    @Value("${images.path}")
    private String imagesPath;

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageViewController.class);

    @GetMapping(
            value = "/images/{id}.jpg",
            produces = MediaType.IMAGE_JPEG_VALUE
    )
    public @ResponseBody byte[] getImageWithMediaType(@PathVariable String id) {
        byte[] image;
        try {
            image = getByteArrayOfImage(id);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("Method: getImageWithMediaType___GET: /images/" + id + "___ ; Image not found");
            throw new ImageNotFoundException();
        }

        LOGGER.info("Method: getImageWithMediaType___GET: /images/" + id + "___ ; Image found and returned.");
        return image;
    }

    private byte[] getByteArrayOfImage(String id) throws ImageNotFoundException, IOException {
        File imageToRead = new File(imagesPath + "/" + id + ".jpg");
        if (!imageToRead.exists()) return new byte[0];

        BufferedImage image = ImageIO.read(new File(imagesPath + "/" + id + ".jpg"));
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", os);

        return os.toByteArray();
    }

}
