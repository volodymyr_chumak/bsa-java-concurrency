package bsa.java.concurrency.image;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Service
public class HashService {
    final private int SCALE_WIDTH = 9;
    final private int SCALE_HEIGHT = 9;

    @Async
    public CompletableFuture<Long> getImageHash(byte[] file) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new ByteArrayInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        long hash = calculateDHash(processImage(image));

        return CompletableFuture.completedFuture(hash);
    }

    private BufferedImage processImage(BufferedImage image) {
        var scaledImage = getScaledImage(image);
        return getGrayscaledImage(scaledImage);
    }

    private Image getScaledImage(BufferedImage image) {
        return image.getScaledInstance(SCALE_WIDTH, SCALE_HEIGHT, Image.SCALE_SMOOTH);
    }

    private BufferedImage getGrayscaledImage(Image image) {
        var grayscaledImage = new BufferedImage(SCALE_WIDTH, SCALE_HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
        grayscaledImage.getGraphics().drawImage(image, 0, 0, null);

        return grayscaledImage;
    }

    private long calculateDHash(BufferedImage image) {
        long hash = 0;
        for (int y = 1; y < 9; y++) {
            for (int x = 1; x < 9; x++) {
                int prev = brightnessScore(image.getRGB(x - 1, y - 1));
                int current = brightnessScore(image.getRGB(x, y));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }

        return hash;
    }

    private int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }
}
