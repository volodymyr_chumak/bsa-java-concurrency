package bsa.java.concurrency.image;

import bsa.java.concurrency.config.AsyncConfiguration;
import bsa.java.concurrency.exception.IncorrectThresholdException;
import bsa.java.concurrency.exception.TooManyImagesException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/image")
public class ImageController {
    @Value("${request.multipartfiles.max-size}")
    private int MAX_SIZE;

    @Autowired
    private ImageService imageService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

    @PostMapping("/batch")
    public ResponseEntity<HttpStatus> batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        if (files.length > MAX_SIZE) {
            LOGGER.error("Method: batchUploadImages___POST: /image/batch;___Too many files to upload;" +
                    "___Uploaded files count: " + files.length);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        LOGGER.info("Method: batchUploadImages___POST: /image/batch;___Uploaded files count: " + files.length);

        try {
            for (MultipartFile f : files) {
                if (!f.isEmpty()) imageService.saveImage(f);
            }
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
                                               @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        if (threshold > 1 || threshold <= 0) {
            LOGGER.error("Method: searchMatches___POST: /search;___Incorrect threshold value;___File size: "
                    + file.getSize() + ";___Threshold= " + threshold);

            throw new IncorrectThresholdException();
        }

        LOGGER.info("Method: searchMatches___POST: /image/search;___File size: " + file.getSize()
                + ";___Threshold= " + threshold);

        CompletableFuture<List<SearchResultDTO>> imagesFuture = imageService.search(file, threshold);

        return imagesFuture.join();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        imageService.deleteById(imageId);
        LOGGER.info("Method: deleteImage___DELETE: /image/" + imageId + " ;");
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        imageService.deleteAll();
        LOGGER.info("Method:purgeImages___DELETE: /image/purge");
    }
}
