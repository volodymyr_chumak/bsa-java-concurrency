package bsa.java.concurrency.image;

import bsa.java.concurrency.entity.Image;
import bsa.java.concurrency.exception.GetBytesException;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class ImageService {

    @Value("${server.address.name}")
    private String serverAddress;

    @Value("${server.port}")
    private String serverPort;

    @Autowired
    private ImageRepository imageRepository;

    public CompletableFuture<Image> saveImage(MultipartFile image) {
        byte[] file = getBytesFromMultipartFile(image);
        return imageRepository.save(file);
    }

    public CompletableFuture<List<SearchResultDTO>> search(MultipartFile imageForSearching, double threshold) {
        byte[] file = getBytesFromMultipartFile(imageForSearching);

        CompletableFuture<Optional<List<SearchResultDTO>>> imagesFuture = imageRepository.search(file, threshold);
        Optional<List<SearchResultDTO>> imagesOpt = imagesFuture.join();

        if (imagesOpt.isEmpty()) {
            CompletableFuture<Image> savedImage = saveImage(imageForSearching);
            return CompletableFuture.completedFuture(new ArrayList<>());
        }

        List<SearchResultDTO> images = imagesOpt.get().parallelStream()
                .map(this::dtoPathToUrlMapper)
                .collect(Collectors.toList());

        return CompletableFuture.completedFuture(images);
    }

    private byte[] getBytesFromMultipartFile(MultipartFile file) {
        Optional<byte[]> bytes;
        try {
            bytes = Optional.of(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            bytes = Optional.empty();
        }
        if (bytes.isEmpty()) throw new GetBytesException();

        return bytes.get();
    }

    private SearchResultDTO dtoPathToUrlMapper(SearchResultDTO dto) {
        dto.setImage("http://" + serverAddress + ":" + serverPort + "/" + dto.getImage());
        return dto;
    }

    public void deleteById(UUID id) {
        imageRepository.delete(id);
    }

    public void deleteAll() {
        imageRepository.deleteAll();
    }

}
