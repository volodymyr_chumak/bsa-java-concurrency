package bsa.java.concurrency.image.dto;

import bsa.java.concurrency.entity.Image;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
public class SearchResultDTO {
    private UUID id;
    private String image;
    private double match;

    public static SearchResultDTO fromEntity(Image image, double matchPercent) {
        return SearchResultDTO.builder()
                .id(image.getId())
                .image(image.getPath())
                .match(matchPercent)
                .build();
    }
}
