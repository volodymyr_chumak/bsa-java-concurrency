package bsa.java.concurrency.image.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class SaveImageResultDto {
    private UUID id;
    private String path;
}
