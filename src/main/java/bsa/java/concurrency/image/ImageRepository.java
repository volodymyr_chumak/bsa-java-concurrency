package bsa.java.concurrency.image;

import bsa.java.concurrency.entity.Image;
import bsa.java.concurrency.exception.GetBytesException;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SaveImageResultDto;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class ImageRepository {
    private final List<Image> images = Collections.synchronizedList(new ArrayList<>());

    @Autowired
    private FileSystem fileSystem;

    @Autowired
    private HashService hashService;

    public CompletableFuture<Image> save(byte[] image) {
        CompletableFuture<SaveImageResultDto> saveResult = fileSystem.saveFile(image);
        CompletableFuture<Long> hash = hashService.getImageHash(image);
        CompletableFuture.allOf(saveResult, hash).join();

        Image savedImage = null;
        try {
            savedImage = Image.fromDto(saveResult.get(), hash.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        saveToList(savedImage);

        return CompletableFuture.completedFuture(savedImage);
    }

    private void saveToList(Image image) {
        images.add(image);
    }

    public CompletableFuture<Optional<List<SearchResultDTO>>> search(byte[] image, double threshold) {
        CompletableFuture<Long> hashFuture = hashService.getImageHash(image);
        Long hash = hashFuture.join();

        Optional<List<SearchResultDTO>> foundedImages = searchInList(hash, threshold);
        if (foundedImages.isEmpty()) return CompletableFuture.completedFuture(Optional.empty());

        return CompletableFuture.completedFuture(foundedImages);
    }

    private Optional<List<SearchResultDTO>> searchInList(long hash, double threshold) {
        List<SearchResultDTO> foundedImages = images.parallelStream()
                .filter(image -> calculateMatchPercent(image.getHash(), hash) >= threshold)
                .map(image -> imageToSearchResultDtoMapper(image, hash))
                .collect(Collectors.toList());

        return foundedImages.isEmpty() ? Optional.empty() : Optional.of(foundedImages);
    }

    private double calculateMatchPercent(long firstImageHash, long secondImageHash) {
        double diffPercent = (double) Long.bitCount(firstImageHash ^ secondImageHash) / 64;
        return 1 - diffPercent;
    }

    private SearchResultDTO imageToSearchResultDtoMapper(Image image, long hash) {
        final double matchPercent = calculateMatchPercent(image.getHash(), hash);
        return SearchResultDTO.fromEntity(image, matchPercent * 100);
    }

    public void delete(UUID imageId) {
        fileSystem.deleteFile(imageId);
        deleteFromList(imageId);
    }

    private void deleteFromList(UUID imageId) {
        images.removeIf(image -> image.getId().equals(imageId));
    }

    public void deleteAll() {
        fileSystem.deleteAllFiles();
        deleteAllFilesFromList();
    }

    private void deleteAllFilesFromList() {
        images.clear();
    }
}
