package bsa.java.concurrency.entity;

import bsa.java.concurrency.image.dto.SaveImageResultDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class Image {
    private UUID id;
    private String path;
    private long hash;

    public static Image fromDto(SaveImageResultDto saveImageDto, Long hash) {
        return Image.builder()
                .id(saveImageDto.getId())
                .path(saveImageDto.getPath())
                .hash(hash)
                .build();
    }
}
