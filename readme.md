# Привіт!
Знебарвлення вирішив робити засобами **BufferedImage** (код нижче), оскільки такий спосіб показує кращу швидкість обробки зображення.

`private BufferedImage getGrayscaledImage(Image image) {
    var grayscaledImage = new BufferedImage(SCALE_WIDTH, SCALE_HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
    grayscaledImage.getGraphics().drawImage(image, 0, 0, null);

    return grayscaledImage;
}`


Пробував алгоритм нижче, але він показує в середньому у 2 рази більший час на обробку(квадратичний час все-таки)

`public static BufferedImage grayscale1(BufferedImage image) {
    int width = image.getWidth();
    int height = image.getHeight();

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int pixel = image.getRGB(x, y);
            int alpha = (pixel >> 24) & 0xff;
            int red = (pixel >> 16) & 0xff;
            int green = (pixel >> 8) & 0xff;
            int blue = pixel & 0xff;

            int avg = (red + green + blue) / 3;

            pixel = (alpha << 24) | (avg << 16) | (avg << 8) | avg;
            image.setRGB(x, y, pixel);
            }
        }

    return image;
}`

